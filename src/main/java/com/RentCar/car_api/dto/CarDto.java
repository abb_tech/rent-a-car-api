package com.RentCar.car_api.dto;

public record CarDto(String model, String brand, int car_year, int price, int km) {
}
