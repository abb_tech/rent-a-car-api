package com.RentCar.car_api.dto;

import com.RentCar.car_api.model.Car;
import com.RentCar.car_api.model.UsersRole;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserDto {


    private String name;

    private String surname;

    private String username;

    private String email;

    private String password;

    private int age;

    private String role ;
}
