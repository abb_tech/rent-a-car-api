package com.RentCar.car_api.repository;

import com.RentCar.car_api.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Integer > {
    Optional<Users> findByUsername(String username);
}
