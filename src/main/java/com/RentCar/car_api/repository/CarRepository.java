package com.RentCar.car_api.repository;

import com.RentCar.car_api.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
}
