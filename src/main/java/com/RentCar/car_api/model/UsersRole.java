package com.RentCar.car_api.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "users")
public class UsersRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public UsersRole(String role, Users users) {
        this.role = role;
        this.users = users;
    }

    @Column(name = "role")
    private String role;
    @OneToOne
    private Users users;
}
