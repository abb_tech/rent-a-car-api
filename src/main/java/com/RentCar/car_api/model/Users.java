package com.RentCar.car_api.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @Column (name = "age")
    private int age;
    @Column (name = "passwords")
    private String password;
    @OneToMany(mappedBy = "user")
    private List<Car> cars;
    @OneToOne(mappedBy = "users",  cascade = CascadeType.ALL)
    private UsersRole role;
}
