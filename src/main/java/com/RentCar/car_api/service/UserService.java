package com.RentCar.car_api.service;

import com.RentCar.car_api.dto.UserDto;
import com.RentCar.car_api.model.Users;
import com.RentCar.car_api.model.UsersRole;
import com.RentCar.car_api.repository.UserRepository;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder bCryptPasswordEncoder;

    public UserDto createUser(UserDto userDto){
        Users user = Users.builder()
                .age(userDto.getAge())
                .surname(userDto.getSurname())
                .name(userDto.getName())
                .username(userDto.getUsername())
                .email(userDto.getEmail())
                .password(bCryptPasswordEncoder.encode(userDto.getPassword()))
                .build();

        UsersRole role = new UsersRole(userDto.getRole(),user);
        user.setRole(role);
        userRepository.save(user);
        return userDto;
    }
}
