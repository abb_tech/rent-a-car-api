package com.RentCar.car_api.service;

import com.RentCar.car_api.dto.CarDto;
import com.RentCar.car_api.model.Car;
import com.RentCar.car_api.model.Users;
import com.RentCar.car_api.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;
    public CarDto addCar(CarDto carDto){
         Car car = Car.builder()
                 .model(carDto.model())
                 .brand(carDto.brand())
                 .carYear(carDto.car_year())
                 .km(carDto.km())
                 .price(carDto.price()).build();
        carRepository.save(car);
         return carDto;
    }

    public List<CarDto>  getAllCars(){
        List<Car> cars = carRepository.findAll();
        if(cars.isEmpty()){
            throw new RuntimeException("NO CARS");
        }
        return cars.stream().map(car -> new CarDto(car.getModel(), car.getBrand(), car.getCarYear(), car.getKm(), car.getPrice())).collect(Collectors.toList());
    }

    public CarDto getCarById(int id){
        Car car = carRepository.findById(id).orElseThrow(()-> new RuntimeException("Car not found with id:" + id));
        return new CarDto(car.getModel(), car.getBrand(), car.getCarYear(),car.getKm(), car.getPrice());
    }

    public CarDto updateCar(CarDto carDto,int id){
        Car car = carRepository.findById(id).orElseThrow();
        car.setKm(carDto.km());
        car.setCarYear(carDto.car_year());
        car.setModel(carDto.model());
        car.setBrand(carDto.brand());
        car.setPrice(carDto.price());
        carRepository.save(car);
        return carDto;
    }

    public String deleteCarById(int id){
        carRepository.deleteById(id);
        return "Car with " + id + " is successfully deleted";
    }
}
