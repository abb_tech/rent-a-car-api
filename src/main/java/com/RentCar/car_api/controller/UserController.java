package com.RentCar.car_api.controller;

import com.RentCar.car_api.dto.UserDto;
import com.RentCar.car_api.service.JWTService;
import com.RentCar.car_api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class UserController {
    private final JWTService jwtService;

    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    @PostMapping("/register")
    public UserDto saveUser(@RequestBody UserDto userRequest) {
        jwtService.generateToken(userRequest.getUsername());
        return userService.createUser(userRequest);
    }
}
