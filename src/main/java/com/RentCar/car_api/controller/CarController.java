package com.RentCar.car_api.controller;

import com.RentCar.car_api.dto.CarDto;
import com.RentCar.car_api.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cars")

public class CarController {

    private final CarService carService;

    @PostMapping("/add")
    private ResponseEntity<CarDto> addCar(@RequestBody CarDto carDto) {
        return new ResponseEntity<>(carService.addCar(carDto), HttpStatus.CREATED);
    }

    @GetMapping()
    private ResponseEntity<List<CarDto>> getAllCars() {
        return new ResponseEntity<>(carService.getAllCars(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarDto> getCarById(@PathVariable int id){
        return new ResponseEntity<>(carService.getCarById(id),HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CarDto> updateCar(@PathVariable int id, @RequestBody CarDto carDto){
        return new ResponseEntity<>(carService.updateCar(carDto, id), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteCar(@PathVariable int id){
        return new ResponseEntity<>(carService.deleteCarById(id), HttpStatus.OK);
    }
}


